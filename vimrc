let mapleader = " "

" Finding files
set path+=**
set wildmenu
set wildignore+=**/node_modules/**
let g:dirvish_relative_paths=1
" nmap <Leader>f :find 
set rtp+=/usr/local/opt/fzf
nmap <Leader>f :FZF<CR>

" File explorer
nmap <Leader>e :Dirvish %:p:h<CR>
" nmap <Leader>e :NERDTree<CR>
nmap <Leader>nd :!mkdir %
nmap <Leader>nf :e %

" Templates
nnoremap <Leader><Leader>b :-1read $HOME/.vim/templates/browserconfig.xml<CR>
nnoremap <Leader><Leader>h :-1read $HOME/.vim/templates/empty.html<CR>7j10la
nnoremap <Leader><Leader>m :-1read $HOME/.vim/templates/manifest.json<CR>
nnoremap <Leader><Leader>p :-1read $HOME/.vim/templates/package.json<CR>

" ALE syntax
let g:ale_fix_on_save = 1
let g:ale_erlang_erlfmt_use_rebar = 1
let g:ale_fixers = {
\ 'elixir': ['mix_format'],
\ 'elm': ['elm-format'],
\ 'erlang': ['erlfmt'],
\ 'javascript': ['prettier'],
\ 'javascriptreact': ['prettier'],
\ 'json': [],
\ 'purescript': ['purty'],
\ 'typescript': ['prettier'],
\ 'typescriptreact': ['prettier'],
\}

let g:ale_linters = {
\ 'elixir': ['credo'],
\ 'elm': ['make'],
\ 'glsl': ['glslang'],
\ 'haskell': ['hlint'],
\ 'html': [],
\ 'javascript': ['prettier'],
\ 'javascriptreact': ['prettier'],
\ 'svelte': [],
\ 'typescript': ['prettier'],
\ 'typescriptreact': ['prettier'],
\ 'vue': ['vls'],
\}
let g:ale_sign_error = 'X'
let g:ale_sign_warning = '!'

autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
highlight ExtraWhitespace ctermbg=red guibg=red

" Show trailing whitespace:
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/

let g:EasyMotion_keys = 'arstdhneioqwfpgjluyzxcvbkm'
let g:EasyMotion_startofline = 0 " Keep cursor column for JK motion

" cnoremap %% <C-R>=fnameescape(expand("%:h")).'/'<CR>
" inoremap # X#

nmap <Leader>d :bd!<CR>
nmap <Leader>h <Plug>(easymotion-linebackward)
nmap <Leader>j <Plug>(easymotion-j)
nmap <Leader>k <Plug>(easymotion-k)
nmap <Leader>l <Plug>(easymotion-lineforward)
nmap <Leader>p :set paste! paste?<CR>
nmap <Leader>q :q<CR>
nmap <Leader>w :w<CR>
nmap <Leader>x :wq<CR>
" nmap s <Plug>(easymotion-sn)

vmap <Leader>t :!column -t<CR>
vmap s :sort<CR>
vmap ' :norm @

nnoremap <C-B> :buffers<CR>:b
nnoremap <C-D> :bd<CR>
nnoremap <C-L> :bn<CR>
nnoremap <C-H> :bp<CR>

au BufNewFile,BufRead *.ck setfiletype chuck
au BufNewFile,BufRead *.csd setfiletype csound
au BufNewFile,BufRead *.glsl setfiletype glsl
au BufNewFile,BufRead *.gren setfiletype gren
au BufNewFile,BufRead *.md set colorcolumn=
au BufNewFile,BufRead *.orc setfiletype csound
au BufNewFile,BufRead *.purs setfiletype purescript
au BufNewFile,BufRead *.sco setfiletype csound
au BufNewFile,BufRead *.syntaxtest setfiletype syntaxtest
au BufNewFile,BufRead *.tsx setfiletype typescriptreact

filetype plugin on
filetype plugin indent on
syntax on

set statusline=[%n]\ %F%=%m%r%w\ \ %l:%c\ \ %P\ 

set autoindent
set autoread
set background=dark
set backspace=eol,indent,start
set buftype=" "
set cinkeys-=0#
set colorcolumn=121
set cursorline
set encoding=utf8
set expandtab
set exrc
set ffs=unix,dos,mac
set gcr=a:blinkon0
set hidden
set history=1000
set ignorecase
set indentkeys-=0#
set infercase
set laststatus=2
set linebreak
set matchtime=5
set mouse=a
set nobackup
set nocompatible
"set noesckeys
set nofoldenable
set nohlsearch
set nostartofline
set noswapfile
set nowb
"set number
set ruler
set scrolloff=10
set secure
set shiftwidth=2
set showcmd
set smartcase
set smarttab
set softtabstop=2
set splitbelow
set splitright
set synmaxcol=240
set tabstop=2
set timeoutlen=200
set wildchar=<Tab>
set wildmode=full
set wrap

colorscheme nord
