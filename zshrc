# If not running interactively, don't do anything
#[[ $- != *i* ]] && return

alias ..='cd ..'

alias l='/bin/ls -FGhl'
alias la='/bin/ls -AFGh'
alias ll='/bin/ls -AFGhl'
alias ls='/bin/ls -FG'
alias ..l='cd .. && ls -FGhl'

autoload -U zmv
autoload -Uz compinit
autoload -Uz vcs_info
zstyle ":vcs_info:*" formats "%F{240}[%F{blue}%b%F{240}]%f"
precmd() { compinit; vcs_info }

export EDITOR='vim'
export GPG_TTY=$(tty)
export HISTCONTROL=erasedups

export HOMEBREW_PREFIX="/opt/homebrew";
export HOMEBREW_CELLAR="/opt/homebrew/Cellar";
export HOMEBREW_REPOSITORY="/opt/homebrew";
export MANPATH="/opt/homebrew/share/man${MANPATH+:$MANPATH}:";
export INFOPATH="/opt/homebrew/share/info:${INFOPATH:-}";
export PATH="/opt/homebrew/bin:/opt/homebrew/sbin${PATH+:$PATH}";

export DENO_INSTALL="/Users/josh/.deno"
export PATH="$DENO_INSTALL/bin:$PATH"

setopt PROMPT_SUBST
PROMPT='%F{cyan}%~${vcs_info_msg_0_}%F{240}$%f '
