if exists("b:current_syntax")
  finish
endif

syntax case match

syntax keyword testComment Comment
highlight link testComment Comment

syntax keyword testConstant Constant
highlight link testConstant Constant

syntax keyword testString String
highlight link testString String

syntax keyword testCharacter Character
highlight link testCharacter Character

syntax keyword testNumber Number
highlight link testNumber Number

syntax keyword testBoolean Boolean
highlight link testBoolean Boolean

syntax keyword testFloat Float
highlight link testFloat Float

syntax keyword testIdentifier Identifier
highlight link testIdentifier Identifier

syntax keyword testFunction Function
highlight link testFunction Function

syntax keyword testStatement Statement
highlight link testStatement Statement

syntax keyword testConditional Conditional
highlight link testConditional Conditional

syntax keyword testRepeat Repeat
highlight link testRepeat Repeat

syntax keyword testLabel Label
highlight link testLabel Label

syntax keyword testOperator Operator
highlight link testOperator Operator

syntax keyword testKeyword Keyword
highlight link testKeyword Keyword

syntax keyword testException Exception
highlight link testException Exception

syntax keyword testPreProc PreProc
highlight link testPreProc PreProc

syntax keyword testInclude Include
highlight link testInclude Include

syntax keyword testDefine Define
highlight link testDefine Define

syntax keyword testMacro Macro
highlight link testMacro Macro

syntax keyword testPreCondit PreCondit
highlight link testPreCondit PreCondit

syntax keyword testType Type
highlight link testType Type

syntax keyword testStorageClass StorageClass
highlight link testStorageClass StorageClass

syntax keyword testStructure Structure
highlight link testStructure Structure

syntax keyword testTypedef Typedef
highlight link testTypedef Typedef

syntax keyword testSpecial Special
highlight link testSpecial Special

syntax keyword testSpecialChar SpecialChar
highlight link testSpecialChar SpecialChar

syntax keyword testTag Tag
highlight link testTag Tag

syntax keyword testDelimiter Delimiter
highlight link testDelimiter Delimiter

syntax keyword testSpecialComment SpecialComment
highlight link testSpecialComment SpecialComment

syntax keyword testDebug Debug
highlight link testDebug Debug

syntax keyword testUnderlined Underlined
highlight link testUnderlined Underlined

syntax keyword testIgnore Ignore
highlight link testIgnore Ignore

syntax keyword testError Error
highlight link testError Error

syntax keyword testTodo Todo
highlight link testTodo Todo

let b:current_syntax = "syntaxtest"
