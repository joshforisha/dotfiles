if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

syntax case match

runtime! $HOME/.config/personal/vim/syntax/csound_opcodes.vim

syn match csOperator "^"
syn match csOperator "<<"
syn match csOperator "<="
syn match csOperator "<"
syn match csOperator "=="
syn match csOperator "="
syn match csOperator ">="
syn match csOperator ">>"
syn match csOperator ">"
syn match csOperator "||"
syn match csOperator "|"
syn match csOperator "-"
syn match csOperator ":"
syn match csOperator "!="
syn match csOperator "?"
syn match csOperator "/"
syn match csOperator "("
syn match csOperator ")"
syn match csOperator "*"
syn match csOperator "&"
syn match csOperator "&&"
syn match csOperator "%"
syn match csOperator "+"

syn match csdTags "^\s*</*CsoundSynthesizer>"
syn region csOptions matchgroup=csdTags start="<CsOptions>" end="</CsOptions>" fold transparent contains=CsComment
syn region csVersion matchgroup=csdTags start="<CsVersion>" end="</CsVersion>" fold transparent contains=CsComment
syn region csLicense matchgroup=csdTags start="<Cs\(Short\|\)License>" end="</Cs\(Short\|\)License>" fold transparent contains=CsComment
syn region csFile matchgroup=csdTags start="<Cs\(File\|FileB\|MidifileB\|SampleB\)>" end="</Cs\(File\|FileB\|MidifileB\|SampleB\)>" fold transparent contains=CsComment
syn region csInstruments matchgroup=csdTags start="<CsInstruments>" end="</CsInstruments>" fold transparent contains=ALLBUT,csScoStatement
syn region csScore matchgroup=csdTags start="<CsScore>" end="</CsScore>" fold transparent contains=CsScoStatement,CsComment,CsString

syn keyword csHeader sr kr ar ksmps nchnls 0dbfs

syn match csVariable "\<[akipSfw]\w\+\>"
syn match csVariable "\<g[akipSfw]\w\+\>"

syn region csInstrRegion matchgroup=csInstrument start="^\s*instr\>" end="^\s*endin\>" fold transparent
syn match csInstrName "\(^\s*instr\s\+\)\@<=\(\w\+,*\s*\)\+"

syn region csOpcodeRegion matchgroup=csInstrument start="^\s*opcode\>" end="^\s*endop\>" fold transparent
syn match csOpcodeName "\(^\s*opcode\s\+\)\@<=\(\S\+\),"

syn keyword CsConditional cggoto cigoto ckgoto cngoto else elseif endif goto if igoto kgoto then tigoto timout
syn keyword CsLoop loop_ge loop_gt loop_le loop_lt until while do od
syn match csLabel "^\s*\<\S\{-}:"
syn match csLabel "\(\<\(goto\|igoto\|kgoto\|tigoto\|reinit\)\s\+\)\@<=\(\w\+\)"
syn match csLabel "\(\<\(cggoto\|cigoto\|ckgoto\|cngoto\)\s\+.\{-},\s*\)\@<=\(\w\+\)"
syn match csLabel "\(\<timout\s\+.\{-},\s*.\{-},\s*\)\@<=\(\w\+\)"

syn match csMacro  +^\s*#\s*\<include\>\s\+.\{-}".\{-}"+
syn region csDefine matchgroup=csMacro start="\(^\s*#\s*define\s\+\w\{-}\s\+\)\@<=#" end="#" fold transparent
syn match csMacro  "^\s*#\(define\|undef\|ifdef\|ifndef\|else\|end\)"
syn match csMacroName "\(^\s*#\(define\|undef\|ifdef\|ifndef\)\s\+\)\@<=\(\w\+\)"
syn match csMacroName "$\w\{-}\.\{,1}\s"

syn match csString +".\{-}"+

syn match csComment ";.*$"
syn region csComment matchgroup=csComment start="/\*" end="\*/" fold

syn match csScoStatement "^\s*f\s*\d\+" " function tables
syn match csScoStatement "^\s*i\s*\d\+" " numbered instrument
syn match csScoStatement +^\s*i\s*"[_a-zA-Z]\w*"+ " named instrument
syn match csScoStatement "^\s*[abemnrstvx]" " score events

hi link csOpcode Label
hi link csOperator Type
hi link csHeader Statement
hi link csInstrument Special
hi link csInstrName Label
hi link csOpcodeName Label
hi link csVariable String
hi link csdSection Label
hi link csdTags Define
hi link csComment Comment
hi link csConditional Conditional
hi link csLoop Repeat
hi link csMacro Define
hi link csMacroName Label
hi link csLabel Define
hi link csString String
hi link csScoStatement Label

let b:current_syntax = "csound"
