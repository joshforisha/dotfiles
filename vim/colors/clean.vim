hi clear

if exists("syntax_on")
  syntax reset
endif

let g:colors_name = "clean"

hi! link Macro Define
hi! link PreCondit PreProc

"hi  ModeMsg
"hi  MoreMsg
"hi  Tag

hi  Comment  ctermfg=DarkGray

hi   Constant   ctermfg=Green
"hi  Boolean
"hi  Character
hi   Float      ctermfg=Magenta
hi   Number     ctermfg=Magenta
"hi  String

hi   Identifier  ctermfg=Cyan  cterm=None
"hi  Function

hi   Statement    ctermfg=Blue
"hi  Conditional
"hi  Repeat
"hi  Label
"hi  Operator
"hi  Keyword
"hi  Exception

hi   PreProc    ctermfg=Blue  cterm=None
"hi  Include
"hi  Define
"hi  Macro
"hi  PreCondit

hi   Type          ctermfg=Blue  cterm=None
"hi  StorageClass
"hi  Structure
"hi  Typedef

hi   Special         ctermfg=Yellow
"hi  Debug
"hi  Delimiter       ctermfg=White
"hi  SpecialChar
"hi  SpecialComment  ctermfg=Cyan    cterm=Italic
"hi  Tag

hi  Underline  cterm=Underline

"hi Ignore

"hi Error

hi  Todo  ctermfg=Black  ctermbg=Yellow  cterm=Bold

hi  ALEError                                            cterm=None
hi  ALEWarning                                          cterm=None
hi  Bold                                                cterm=Bold
hi  ColorColumn     ctermfg=None      ctermbg=Black
hi  Constant        ctermfg=Green
hi  Cursor                            ctermbg=None
hi  CursorColumn    ctermfg=None      ctermbg=Black
hi  CursorLine      ctermfg=None      ctermbg=None      cterm=None
hi  CursorLineNr    ctermfg=None      ctermbg=Black     cterm=None
hi  Directory       ctermfg=Cyan      ctermbg=None
hi  EndOfBuffer     ctermfg=Black     ctermbg=None
hi  ErrorMsg        ctermfg=None      ctermbg=Red
hi  FoldColumn      ctermfg=Black     ctermbg=None
hi  Folded          ctermfg=Black     ctermbg=None
hi  Identifier      ctermfg=Cyan                        cterm=None
hi  IncSearch       ctermfg=None      ctermbg=Cyan      cterm=None
hi  Italic                                              cterm=Italic
hi  LineNr          ctermfg=DarkGray  ctermbg=None
hi  MatchParen      ctermfg=Black     ctermbg=Cyan
hi  NonText         ctermfg=DarkGray
hi  Normal          ctermfg=None      ctermbg=None
hi  PMenu           ctermfg=None      ctermbg=Black     cterm=None
hi  PMenuSel        ctermfg=Cyan      ctermbg=DarkGray
hi  PmenuSbar       ctermfg=None      ctermbg=Black
hi  PmenuThumb      ctermfg=None      ctermbg=DarkGray
hi  Question        ctermfg=None
hi  Search          ctermfg=Black     ctermbg=Cyan      cterm=None
hi  SignColumn      ctermfg=Black     ctermbg=None
hi  SpecialKey      ctermfg=DarkGray
hi  SpellBad                          ctermbg=None      cterm=Undercurl
hi  SpellCap                          ctermbg=None      cterm=Undercurl
hi  SpellLocal                        ctermbg=None      cterm=Undercurl
hi  SpellRare                         ctermbg=None      cterm=Undercurl
hi  StatusLine      ctermfg=Cyan      ctermbg=Black     cterm=None
hi  StatusLineNC    ctermfg=Cyan      ctermbg=None      cterm=None
hi  TabLine         ctermfg=None      ctermbg=Black     cterm=None
hi  TabLineFill     ctermfg=None      ctermbg=Black     cterm=None
hi  TabLineSel      ctermfg=Cyan      ctermbg=DarkGray  cterm=None
hi  Title           ctermfg=None                        cterm=None
hi  VertSplit       ctermfg=DarkGray  ctermbg=Black     cterm=None
hi  Visual                            ctermbg=Black
hi  VisualNOS                         ctermbg=Black
hi  WarningMsg      ctermfg=Black     ctermbg=Yellow
hi  WildMenu        ctermfg=Cyan      ctermbg=DarkGray
hi  iCursor                           ctermbg=None
