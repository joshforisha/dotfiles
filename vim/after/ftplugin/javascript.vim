:hi Function ctermfg=none
:setlocal formatoptions-=c
:setlocal formatoptions-=r
:setlocal formatoptions-=o

:nmap <Leader>r :w<CR>:!node %:p<CR>
