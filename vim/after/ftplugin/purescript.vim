:nmap <buffer> <Leader>b :w<CR>:!spago build<CR>
:nmap <buffer> <Leader>t :w<CR>:!spago test<CR>

set shiftwidth=4
set softtabstop=4
set tabstop=4
