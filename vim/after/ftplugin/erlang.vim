:nmap <buffer> <Leader>u :w<CR>:!rebar3 ex_doc<CR>

set colorcolumn=81
set shiftwidth=4
set softtabstop=4
set tabstop=4
