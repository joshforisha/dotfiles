# Josh's Dotfiles

## Git

Create `~/.gitconfig`:

```
[include]
  path = <DOTFILES>/gitconfig
```

### Vim

Symlink `vim/` directory and `vimrc` to home directory:

- `ln -s <DOTFILES>/vim ~/.vim`
- `ln -s <DOTFILES>/vimrc ~/.vimrc`
