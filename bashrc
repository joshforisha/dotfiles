# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PLATFORM=`uname`

alias ..='cd ..'
alias vimdiff='vim `git diff --names-only`'

if [[ "$PLATFORM" == 'Darwin' ]]; then
    alias l='/bin/ls -FGhl'
    alias la='/bin/ls -AFGh'
    alias ll='/bin/ls -AFGhl'
    alias ls='/bin/ls -FG'
    alias ..l='cd .. && ls -FGhl'
else
    alias l='/bin/ls -Fhl --color=auto'
    alias la='/bin/ls -AFh --color=auto'
    alias ll='/bin/ls -AFhl --color=auto'
    alias ls='/bin/ls -F --color=auto'
    alias ..l='cd .. && ls -Fhl --color=auto'
fi

stty -ixon

source ~/.config/dotfiles/colors.sh

BRANCH=$'\uf418'

function git_branch() {
  br=`git branch 2> /dev/null | sed -e '/^[^*]/d' -e "s/* \(.*\)/$BRANCH \1 /"`
  if [ "$br" == " " ]
    then echo ""
    else echo "$br"
  fi
}

export EDITOR="vim"
export HISTCONTROL=erasedups
export PATH="~/.local/bin:$PATH"

export PS1="$CN4\u$CN8@\h $CN2\w $CN3\$(git_branch)$CN8\$$CNN "
